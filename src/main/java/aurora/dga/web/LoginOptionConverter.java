/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.dga.web;

import aurora.dga.web.UserBean.LoginOption;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 *
 * @author Luis Caamaño
 */
@ManagedBean(name="loginOptionConverter")
@RequestScoped
//@Named(value = "loginOptionConverter")
//@Dependent
public class LoginOptionConverter implements Converter {

    /**
     * Creates a new instance of LoginOptionConverter
     */
    public LoginOptionConverter() {
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.isEmpty()) {
            return null;
        }
        
        try {
            return submittedValue;
        } catch (NumberFormatException e) {
            throw new ConverterException(new FacesMessage(String.format("%s no es una opcion de Login valida", submittedValue)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
        if (modelValue == null) {
            return "";
        }

        if (modelValue instanceof LoginOption) {
         
            //return String.valueOf(((User) modelValue).getId());
            return String.valueOf(((LoginOption) modelValue).getOptionName());
        } else if(modelValue instanceof String){
            return modelValue.toString();
        } else
            {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Option ", modelValue)));
        }
    }
    
}
