/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.dga.web;

import Navixy.NavixyTracker;
import aurora.dga.web.UserBean.LoginOption;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 *
 * @author Luis Caamaño
 */
@ManagedBean(name="trackerConverter")
@RequestScoped
public class TrackerConverter implements Converter {

    /**
     * Creates a new instance of trackerConverter
     */
    public TrackerConverter() {
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        if (submittedValue == null || submittedValue.isEmpty()) {
            return null;
        }
        
        try {
        Gson gson = new Gson();
        Type listType = new TypeToken<NavixyTracker>(){}.getType();
        //String content = gson.toJson(jsonResponse.getList()).toString();

        return gson.fromJson(submittedValue, listType);
            //return (NavixyTracker)submittedValue;
        } catch (NumberFormatException e) {
            throw new ConverterException(new FacesMessage(String.format("%s no es una opcion de Tracker valida", submittedValue)), e);
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
        if (modelValue == null) {
            return "";
        }

        if (modelValue instanceof NavixyTracker) {
         
            //return String.valueOf(((User) modelValue).getId());
            
            Gson gson = new Gson();
            Type listType = new TypeToken<NavixyTracker>(){}.getType();
            String content = gson.toJson(modelValue).toString();
            return content;
            //return String.valueOf(((NavixyTracker) modelValue));
        } else if(modelValue instanceof String){
            return modelValue.toString();
        } else
            {
            throw new ConverterException(new FacesMessage(String.format("%s is not a valid Tracker Option ", modelValue)));
        }
    }
    
}
