package aurora.dga.web;

import Navixy.NavixyAPICalls;
import static Navixy.NavixyAPICalls.listNavixyTrackers;
import static Navixy.NavixyAPICalls.navixyGetTrackList;
import static Navixy.NavixyAPICalls.navixyGetTrackers;
import Navixy.NavixyJsonResponse;
import Navixy.NavixyTrack;
import Navixy.NavixyTracker;
import Navixy.NavixyTrackerState;
import Navixy.NavixyUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "status", eager = true)
public class StatusBean {
    public StatusBean() {
        System.out.println("StatusBean instantiated");
    }
//    public String getMessage() throws NoSuchAlgorithmException, IOException {
//
//        NavixyJsonResponse user;
//        user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        
//        return user.toString();
//    }
    
//        public ArrayList<NavixyTracker> getTrackers() throws NoSuchAlgorithmException, IOException {
//
//        NavixyJsonResponse user;
//        user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        ArrayList<NavixyTracker> navixyTrackers = NavixyAPICalls.navixyGetTrackers(user.getHash());
//
//        return navixyTrackers;
//    }
        
//        public ArrayList<NavixyTrack> getTracks() throws NoSuchAlgorithmException, IOException {
//            
//            String dateFrom = "2018-10-18 03:39:44";
//            String dateTo = "2018-12-24 03:39:44";
//            
//
//        
//        NavixyJsonResponse user;
//        user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        ArrayList<NavixyTracker> navixyTrackers = NavixyAPICalls.navixyGetTrackers(user.getHash());
//        NavixyTracker navixyTracker = navixyTrackers.get(0);
//        String trackerID = navixyTracker.getId();
//        
//        ArrayList<NavixyTrack> trackList = navixyGetTrackList(user.getHash(), trackerID, dateFrom, dateTo);
//
//        
//        
//       // ArrayList<NavixyTrack> navixyTracks = NavixyAPICalls.navixyGetTrackList(user.getHash(), );
//
//        return trackList;
//    }     
        
        
        
//    public String getOpen() {
//
//        System.out.println("################################################################");
//        System.out.println("###########################   CANDADO ##########################");
//        System.out.println("################################################################");
//        NavixyJsonResponse user = null;
//        try {
//            user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        } catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        ArrayList<NavixyTracker> navixyTrackers = null;
//        try {
//            navixyTrackers = navixyGetTrackers(user.getHash());
//        } catch (IOException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        NavixyTracker navixyTracker = navixyTrackers.get(0);
//        String trackerID = navixyTracker.getId();
//        try {
//            NavixyJsonResponse ff = NavixyAPICalls.navixyElectronicLockOpenCommand(user.getHash(), trackerID, "888888");
//        } catch (Exception ex) {
//            return "failure";
//        }
//
//        
//        return "success";
//    }   
        
    
    public ArrayList<NavixyTrackerState> getState() {
    //public NavixyTrackerState getState() {

        System.out.println("################################################################");
        System.out.println("###########################   STATE   ##########################");
        System.out.println("################################################################");
        
        NavixyUser navixyUser;
        
                ArrayList<NavixyTrackerState> arrayList = new ArrayList<>();

        
        try {
            
            Map<String, Object> user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
            
            navixyUser = new Gson().fromJson(user.toString(), new TypeToken<NavixyUser>() {
            }.getType());        
            
            System.out.println("LOGIN USER HASH " + navixyUser.toString());
            ArrayList<NavixyTracker> navixyTrackers = null;
            navixyTrackers = listNavixyTrackers(navixyGetTrackers(navixyUser.getHash()));
            NavixyTracker navixyTracker = navixyTrackers.get(0);
            String trackerID = navixyTracker.getId();
            
            NavixyTrackerState trackerState = null;
        
            trackerState = (NavixyTrackerState)NavixyAPICalls.navixyTrackerGetState(navixyUser.getHash(), trackerID);
            arrayList.add(trackerState);

                        
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
        }

 
      

        return arrayList;
        //return trackerState;
    }   
    
}
