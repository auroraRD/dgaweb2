package aurora.dga.web;

import Navixy.NavixyAPICalls;
import static Navixy.NavixyAPICalls.listNavixyTrackers;
import static Navixy.NavixyAPICalls.navixyGetTrackList;
import static Navixy.NavixyAPICalls.navixyGetTrackers;
import Navixy.NavixyJsonResponse;
import Navixy.NavixyTrack;
import Navixy.NavixyTracker;
import Navixy.NavixyUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "welcome", eager = true)
public class WelcomeBean {
    
    
    public ArrayList<NavixyTracker> navixyTrackers;
    public ArrayList<NavixyTrack> trackList;
    String dateFrom = "2018-10-18 03:39:44";
    String dateTo = "2018-12-24 03:39:44";
    //NavixyTracker navixyTracker;
    
    public WelcomeBean() {
        
        System.out.println("NEWWWW WelcomeBean instantiated");
        
        
        NavixyUser navixyUser;

        
        
        try {

            Map<String, Object> user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
            
            navixyUser = new Gson().fromJson(user.toString(), new TypeToken<NavixyUser>() {
            }.getType());        
            
            navixyTrackers = listNavixyTrackers( NavixyAPICalls.navixyGetTrackers(navixyUser.getHash()));
            NavixyTracker navixyTracker = navixyTrackers.get(0);
            String trackerID = navixyTracker.getId();
            trackList = navixyGetTrackList(navixyUser.getHash(), trackerID, dateFrom, dateTo);

            
            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
        }
           
    }
           
    public ArrayList<NavixyTracker> getTrackers(String trackerr) throws NoSuchAlgorithmException, IOException {
        return navixyTrackers;
    }
        
    public ArrayList<NavixyTrack> getTracks() throws NoSuchAlgorithmException, IOException {
        return trackList;
    }     
        
        
        
    public String getCandado() {

        System.out.println("################################################################");
        System.out.println("###########################   CANDADO ##########################");
        System.out.println("################################################################");

        NavixyTracker navixyTracker = navixyTrackers.get(0);
        String trackerID = navixyTracker.getId();
        try {
           // NavixyJsonResponse ff = NavixyAPICalls.navixyElectronicLockOpenCommand(user.getHash(), trackerID, "888888");
        } catch (Exception ex) {
            return "failure";
        }
        
        return "success";
    }   
        
}
