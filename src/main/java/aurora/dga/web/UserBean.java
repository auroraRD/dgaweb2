/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.dga.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Luis Caamaño
 */

//@Named(value="usuario")
//@ApplicationScoped
@ManagedBean(name="UserBean")
@ViewScoped
public class UserBean implements Serializable {
//   @ManagedProperty(value = "#{param.clientname}")
//   private String clientname;
    
    
    private String login;
    private String password;
    private LoginOption selectedLogin = new LoginOption("", "", "");
    private List<LoginOption> loginOptions;
    private String passcode;

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public class LoginOption {
        private String optionName;
        private String optionDesc;
        private String loginHash;

        public LoginOption(String optionName, String optionDesc, String loginHash) {
            this.optionName = optionName;
            this.optionDesc = optionDesc;
            this.loginHash = loginHash;
        }
        
        public String getOptionName() {
            return optionName;
        }

        public void setOptionName(String optionName) {
            this.optionName = optionName;
        }

        public String getOptionDesc() {
            return optionDesc;
        }

        public void setOptionDesc(String optionDesc) {
            this.optionDesc = optionDesc;
        }

        public String getLoginHash() {
            return loginHash;
        }

        public void setLoginHash(String loginHash) {
            this.loginHash = loginHash;
        }

    }
  
    public UserBean() {
        	this.loginOptions = new ArrayList<LoginOption>();
                //this.loginOptions.add(new LoginOption("Aurora", "Punto Do Technologies","d5fd55ee116f9b2118a4ad114d04f554"));
		//this.loginOptions.add(new LoginOption("DGA", "Direccion General de Aduanas - ReComTAC","7f806ad677f1463042dfd054fc7e6961"));
		this.loginOptions.add(new LoginOption("Pre-Instalacion", "Pre-Instalacion - Cuenta Interna","304b67b1bc816c0e807ac2427987f014"));
		//this.loginOptions.add(new LoginOption("A24", "Alarma 24","7b840902e7b7c6fcbf575de45ea51549"));
		this.loginOptions.add(new LoginOption("KM100", "Kilometro 100","db24fc84bcab725e0ba90faf700b9260"));
    }  

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
        
    public List<LoginOption> getLoginOptions() {
        return loginOptions;
    }

    public void setLoginOptions(List<LoginOption> loginOptions) {
        this.loginOptions = loginOptions;
    }
        
    public LoginOption getSelectedLogin() {
        return selectedLogin;
    }

    public void setSelectedLogin(LoginOption selectedLogin) {
        this.selectedLogin = selectedLogin;
    }

}
