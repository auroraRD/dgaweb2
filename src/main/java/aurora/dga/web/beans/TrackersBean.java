package aurora.dga.web.beans;

import Navixy.NavixyAPICalls;
import static Navixy.NavixyAPICalls.listNavixyTrackers;
import static Navixy.NavixyAPICalls.navixyGetTrackList;
import static Navixy.NavixyAPICalls.navixyGetTrackers;
import Navixy.NavixyJsonResponse;
import Navixy.NavixyTrack;
import Navixy.NavixyTracker;
import Navixy.NavixyUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

@ManagedBean(name = "trackers", eager = true)
public class TrackersBean {
    
    private NavixyTracker selectedTracker;          
    private ArrayList<NavixyTracker> trackerList;
    private Map<String,Object> user;
    private String hash;    
    private String selectedTrackerId;        
    private String originLocation;
    private String originDate;
    private String destinyLocation;
    private String destinyDate;
    
    
        public TrackersBean() throws NoSuchAlgorithmException, IOException {
            System.out.println("TrackersBean instanciado"); 
        
                user = NavixyAPICalls.navixyAPILogin("lcaamano@aurora.com.do", "Lc123456");
                NavixyUser navixyUser = new Gson().fromJson(user.toString(), new TypeToken<NavixyUser>() {
                }.getType());
                String userHash = navixyUser.getHash();
                
// TRACKERS #####################################
        Gson gson = new Gson();
        Map<String, Object> navixyResponseMap = navixyGetTrackers(userHash);                                                // Recibo el Mapa de Respuesta del Servicio
//        String apiJsonOutput = gson.toJson(navixyResponseMap);                                                              // Convierto a String JSON el Mapa
//        NavixyJsonResponse jsonResponse = gson.fromJson(apiJsonOutput, new TypeToken<NavixyJsonResponse>() {
//        }.getType());    // Convierto el JSON a Objeto Respuesta
//        ArrayList<Object> tempObjectList = jsonResponse.getList();                                                          // Le saco la lista de respuesta al Objeto
//        apiJsonOutput = gson.toJson(tempObjectList);                                                                        // Convierto a Lista a String JSON
//        ArrayList<NavixyTracker> trackerList = new Gson().fromJson(apiJsonOutput, new TypeToken<ArrayList<NavixyTracker>>() {
//        }.getType());  // Convierto el JSON a Objeto Lista de Trackers

        trackerList = listNavixyTrackers(navixyResponseMap);

                //trackerList = NavixyAPICalls.navixyGetTrackers(hash);
        trackerList.removeIf(n -> (! n.getSource().getModel().equals("jointech_jt701")));  // Remover todo tracker que no sea un candado

        System.out.println("TrackersBean fin"); 

        }
     


    
//    
//    public String getServerLocale(){
//        
//        Locale browserLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
//        return browserLocale.;
//    }
        
   public String validate() {
    boolean valid ;
    //valid = LoginDAO.validate(user, pwd);
      valid = true;
     if(valid){
         HttpSession session = SessionUtils.getSession();
         session.setAttribute("tracker", selectedTrackerId);
         ArrayList<NavixyTracker>  trackerList2 = trackerList;
         trackerList2.removeIf(n -> (! n.getId().equals(selectedTrackerId)));
         selectedTracker=trackerList2.get(0);
         return "etapa1";
     } else {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Tracker Erroneo"));
         return "admin";
    }
 }



    
    public String getSelectedTrackerId() {
        return selectedTrackerId;
    }

    public void setSelectedTrackerId(String selectedTrackerId) {
        this.selectedTrackerId = selectedTrackerId;
    }

    public NavixyTracker getSelectedTracker() {
        return selectedTracker;
    }

    public void setSelectedTracker(NavixyTracker selectedTracker) {
        this.selectedTracker = selectedTracker;
    }
    
    public ArrayList<NavixyTracker> getTrackerList() {
        return trackerList;
    }

    public void setTrackerList(ArrayList<NavixyTracker> trackerList) {
        this.trackerList = trackerList;
    }



    public Map<String, Object> getUser() {
        return user;
    }

//    @ManagedProperty(value = "#{param.clientname}")
//    private String clientname;
    //    public String getClientname() {
//        return clientname;
//    }
//
//    public void setClientname(String clientname) {
//        this.clientname = clientname;
//    }
//    public String getMessage() throws NoSuchAlgorithmException, IOException {
//
//        NavixyJsonResponse user;
//        user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        
//        return user.toString();
//    }
//        public ArrayList<NavixyTracker> getTrackers() throws NoSuchAlgorithmException, IOException {
//
//        NavixyJsonResponse user;
//        user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        ArrayList<NavixyTracker> navixyTrackers = NavixyAPICalls.navixyGetTrackers(user.getHash());
//
//        return navixyTrackers;
//    }
//        public ArrayList<NavixyTrack> getTracks() throws NoSuchAlgorithmException, IOException {
//            
//            String dateFrom = "2018-10-18 03:39:44";
//            String dateTo = "2018-12-24 03:39:44";
//
//        
//
//        NavixyJsonResponse user;
//        user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        ArrayList<NavixyTracker> navixyTrackers = NavixyAPICalls.navixyGetTrackers(user.getHash());
//        NavixyTracker navixyTracker = navixyTrackers.get(0);
//        String trackerID = navixyTracker.getId();
//        
//        ArrayList<NavixyTrack> trackList = navixyGetTrackList(user.getHash(), trackerID, dateFrom, dateTo);
//
//
//        
//       // ArrayList<NavixyTrack> navixyTracks = NavixyAPICalls.navixyGetTrackList(user.getHash(), );
//
//        return trackList;
//    }
//    public String getOpen() {
//
//        System.out.println("################################################################");
//        System.out.println("###########################   CANDADO ##########################");
//        System.out.println("################################################################");
//        NavixyJsonResponse user = null;
//        try {
//            user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        } catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        ArrayList<NavixyTracker> navixyTrackers = null;
//        try {
//            navixyTrackers = navixyGetTrackers(user.getHash());
//        } catch (IOException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        NavixyTracker navixyTracker = navixyTrackers.get(0);
//        String trackerID = navixyTracker.getId();
//        try {
//            NavixyJsonResponse ff = NavixyAPICalls.navixyElectronicLockOpenCommand(user.getHash(), trackerID, "888888");
//        } catch (Exception ex) {
//            return "failure";
//        }
//
//        
//        return "success";
//    }
//    public String open() {
//
//        System.out.println("################################################################");
//        System.out.println("###########################   CANDADO ##########################");
//        System.out.println("################################################################");
//        NavixyJsonResponse user = null;
//        try {
//            user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
//        } catch (NoSuchAlgorithmException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        // user = navixyAPILogin("rbeltre@a24.com.do", "Richard");
//        System.out.println("LOGIN USER HASH " + user.toString());
//        ArrayList<NavixyTracker> navixyTrackers = null;
//        try {
//            navixyTrackers = navixyGetTrackers(user.getHash());
//        } catch (IOException ex) {
//            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        NavixyTracker navixyTracker = navixyTrackers.get(0);
//        String trackerID = navixyTracker.getId();
//        try {
//            //NavixyJsonResponse ff = NavixyAPICalls.navixyElectronicLockOpenCommand(user.getHash(), trackerID, "888888");
//        } catch (Exception ex) {
//            this.setActividad("Candado Inaccesible");
//            return "failure";
//        }
//
//        this.setActividad("Candado Activo");
//        return "success";
//    }
    public void setUser(Map<String, Object> user) {
        this.user = user;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getOriginLocation() {
        return originLocation;
    }

    public void setOriginLocation(String originLocation) {
        this.originLocation = originLocation;
    }

    public String getOriginDate() {
        return originDate;
    }

    public void setOriginDate(String originDate) {
        this.originDate = originDate;
    }

    public String getDestinyLocation() {
        return destinyLocation;
    }

    public void setDestinyLocation(String destinyLocation) {
        this.destinyLocation = destinyLocation;
    }

    public String getDestinyDate() {
        return destinyDate;
    }

    public void setDestinyDate(String destinyDate) {
        this.destinyDate = destinyDate;
    }
    
    
    
    
}
