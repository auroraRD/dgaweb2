/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.dga.web.beans;

import aurora.dga.dao.UserLogin;
import aurora.dga.dao.LoginDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Luis Caamaño
 */

//@Named(value="usuario")
//@ApplicationScoped
@ManagedBean(name="login")
@SessionScoped
public class LoginBean implements Serializable {
    
   private static final long serialVersionUID = 1L;
//   @ManagedProperty(value = "#{param.clientname}")
//   private String clientname;

    public LoginBean() {}  
    
private Long id;    
private String username;
private String password;
private String firstName;
private String lastName;
private String address;
private String clientType;


private String msg;

//TODO: Aplicar Hash Salting a password

 public String validate() {
   UserLogin valid;
      valid = LoginDAO.validate(username, password);
      //valid = true;
     if(valid!=null){
         HttpSession session = SessionUtils.getSession();
         session.setAttribute("username", valid.getLogin());
         session.setAttribute("firstname", valid.getFirstName());
         session.setAttribute("lastname", valid.getLastName());
         session.setAttribute("address", valid.getAddress());
         session.setAttribute("clienttype", valid.getClientType());
         
         firstName=valid.getFirstName();
         lastName=valid.getLastName();
         address=valid.getAddress();
         clientType=valid.getClientType();
         
         
         return "admin";
     } else {
         FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Login o Password Erroneos"));
         return "login";
    }
 }

	public String logout() {
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		return "login";
	}
 
 
     public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

 
 
}
