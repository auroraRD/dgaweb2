package aurora.dga.web.beans;


import static Navixy.NavixyAPICalls.getNavixyTrackerState;
import static Navixy.NavixyAPICalls.listNavixyTrackers;
import static Navixy.NavixyAPICalls.navixyAPILogin;
import static Navixy.NavixyAPICalls.navixyGetTrackers;
import static Navixy.NavixyAPICalls.navixyTrackerGetState;
import Navixy.NavixyTracker;
import Navixy.NavixyTrackerState;
import Navixy.NavixyUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.event.ItemSelectEvent;
import org.primefaces.model.chart.*;


@ManagedBean
public class ChartView implements Serializable {
 

    private MeterGaugeChartModel meterGaugeModel1;
    private MeterGaugeChartModel meterGaugeModel2;
    
    ArrayList<NavixyTracker> navixyTrackers;
    String userHash;

    public ArrayList<NavixyLockBattStatus> getBattStatuses() {
        return battStatuses;
    }

    public void setBattStatuses(ArrayList<NavixyLockBattStatus> battStatuses) {
        this.battStatuses = battStatuses;
    }
  
    public ArrayList<NavixyLockBattStatus> battStatuses;
    
    public class NavixyLockBattStatus implements Serializable {
        
        private Navixy.NavixyTracker navixyTracker;
        private MeterGaugeChartModel chartModel;

        public NavixyTracker getNavixyTracker() {
            return navixyTracker;
        }

        public void setNavixyTracker(NavixyTracker navixyTracker) {
            this.navixyTracker = navixyTracker;
        }

        public MeterGaugeChartModel getChartModel() {
            return chartModel;
        }

        public void setChartModel(MeterGaugeChartModel chartModel) {
            this.chartModel = chartModel;
        }
        
        
        
        
    }
 
    
    public void initNavixyTrackers() throws NoSuchAlgorithmException, IOException {
        
        Map<String, Object> user;

//        user = navixyAPILogin("lcaamano@aurora.com.do", "Lc123456");
        //user = navixyAPILogin("test@DGA.com", "1234567");
        user = navixyAPILogin("supervision@aduanas.gob.do", "R3c0mt4c$");
        
        NavixyUser navixyUser = new Gson().fromJson(user.toString(), new TypeToken<NavixyUser>() {}.getType());

        userHash = navixyUser.getHash();
        
        Map<String, Object> navixyResponseMap = navixyGetTrackers(userHash);

        navixyTrackers = listNavixyTrackers(navixyResponseMap);
        
        //navixyTrackers.s
        
    }
    
    
    @PostConstruct
    public void init() {
        
        System.out.println("Chart Bean init");
        
        try {
            initNavixyTrackers();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ChartView.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ChartView.class.getName()).log(Level.SEVERE, null, ex);
        }

        createMeterGaugeModels();
        
        createArrayMeterGaugeModels();
 
        System.out.println("Chart Bean end");

    }
 
    public void itemSelect(ItemSelectEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Item selected",
                "Item Index: " + event.getItemIndex() + ", Series Index:" + event.getSeriesIndex());
 
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    
 
 
    
 
    
 
 
    
 
   

 

 
  
 
  
 
   
  
  
 
   
 
 
    
 
    private MeterGaugeChartModel initMeterGaugeModel() {
        List<Number> intervals = new ArrayList<Number>() {
            {
                add(20);
                add(50);
                add(100);
                //add(220);
            }
        };
 
        return new MeterGaugeChartModel(100, intervals);
    }
 
    private void createMeterGaugeModels() {
        meterGaugeModel1 = initMeterGaugeModel();
        meterGaugeModel1.setTitle("MeterGauge Chart");
        meterGaugeModel1.setGaugeLabel("km/h");
        meterGaugeModel1.setGaugeLabelPosition("bottom");
 
        meterGaugeModel2 = initMeterGaugeModel();
        meterGaugeModel2.setTitle("Custom Options");
        meterGaugeModel2.setSeriesColors("66cc66,93b75f,E7E658,cc6666");
        meterGaugeModel2.setGaugeLabel("km/h");
        meterGaugeModel2.setGaugeLabelPosition("bottom");
        meterGaugeModel2.setShowTickLabels(false);
        meterGaugeModel2.setLabelHeightAdjust(10);
        //meterGaugeModel2.setIntervalOuterRadius(100);
    }
    
    private void createArrayMeterGaugeModels() {
        ArrayList<NavixyLockBattStatus> battStatusesTmp;
        battStatusesTmp = new ArrayList<NavixyLockBattStatus>();
        
        for(int i=0;i<navixyTrackers.size();i++){
        //for(int i=0;i<2;i++){
            
            NavixyLockBattStatus battStatusTmp = new NavixyLockBattStatus();
            
            NavixyTracker navixyTracker = navixyTrackers.get(i);
            
            
            MeterGaugeChartModel meterGaugeModelTmp = initMeterGaugeModel();
            //meterGaugeModelTmp.setTitle("MeterGauge Chart");
            //meterGaugeModelTmp.setGaugeLabel("km/h");
            meterGaugeModelTmp.setGaugeLabel("Bateria");
            meterGaugeModelTmp.setGaugeLabelPosition("bottom");
            
            Map<String, Object> rrr = null;
            
            try {
                rrr = navixyTrackerGetState(userHash, navixyTracker.getId());
            } catch (IOException ex) {
                Logger.getLogger(ChartView.class.getName()).log(Level.SEVERE, null, ex);
            }
                                            
            Object objTrackerState = rrr.get("state");                                       // obtengo el mapa con el state                                        
                                            
            NavixyTrackerState navixyTrackerState = getNavixyTrackerState((Map<String, Object>)objTrackerState);
            String state = navixyTrackerState.getBattery_level();
            meterGaugeModelTmp.setValue(Double.parseDouble(state));
            //meterGaugeModelTmp.setValue(navixyTracker.getSource().);
            
            
            battStatusTmp.setNavixyTracker(navixyTracker);
            battStatusTmp.setChartModel(meterGaugeModelTmp);

            battStatusesTmp.add(battStatusTmp);
            
            Collections.sort(battStatusesTmp, new Comparator<NavixyLockBattStatus>() 
            {
                @Override
                public int compare(NavixyLockBattStatus o1, NavixyLockBattStatus o2) {

                    return o1.getChartModel().getValue().intValue() > o2.getChartModel().getValue().intValue() ? 1 : (o1.getChartModel().getValue().intValue() < o2.getChartModel().getValue().intValue()) ? -1 : 0; 

                }
                
            }
            );
            
            this.setBattStatuses(battStatusesTmp);
        }
        
        //battStatusesTmp.  add(meterGaugeModel1);
       

//        meterGaugeChartModelsTmp.add(meterGaugeModel2);
//       
//        meterGaugeChartModelsTmp.add(meterGaugeModel1);
//        meterGaugeChartModels = meterGaugeChartModelsTmp;
        
        

}
 
 
}