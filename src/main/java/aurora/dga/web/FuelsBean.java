package aurora.dga.web;

import Navixy.NavixyAPICalls;
import static Navixy.NavixyAPICalls.*;
import Navixy.NavixyAPIFuelCalls;
import Navixy.NavixyJsonResponse;
import Navixy.NavixyTrack;
import Navixy.NavixyTracker;
import Navixy.NavixyTrackerState;
import Navixy.NavixyUser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.Array;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "fuels", eager = true)
public class FuelsBean {
    public FuelsBean() {
        System.out.println("FuelBean instantiated");
    }
    
    
private static <V, K> Map<V, K> invert(Map<K, V> map) {

    Map<V, K> inv = new HashMap<V, K>();

    for (Entry<K, V> entry : map.entrySet())
        inv.put(entry.getValue(), entry.getKey());

    return inv;
}
    
    public String getFuelChartData() throws IOException{
                String userHash = "dd67bd83b7ccc3040b6c8e887d799369";
                String trackerID = "366988";
        
                Map<String, Object> trackerSchedule;
                trackerSchedule = NavixyAPIFuelCalls. navixyGetScheduleList2(userHash, trackerID);
                Map<String, Object> dataFuel = (Map<String, Object>)trackerSchedule.get("report");
                ArrayList rdataFuel;
                rdataFuel =  (ArrayList)dataFuel.get("sheets");
                dataFuel = (Map<String, Object>) rdataFuel.get(0);
                rdataFuel =  (ArrayList) dataFuel.get("sections");
                dataFuel = (Map<String, Object>) rdataFuel.get(0);
                rdataFuel =  (ArrayList) dataFuel.get("data_sets");
                dataFuel = (Map<String, Object>) rdataFuel.get(0);
                //Map<String, Object> dataFuelInv= invert((Map<K, V>) dataFuel);
                
                rdataFuel =  (ArrayList) dataFuel.get("points");
                
                
                
                String ss="[";
                for(int i=0;i<rdataFuel.size();i++){
                    dataFuel=(Map<String, Object>) rdataFuel.get(i);
                    ss+="[";
                    ss+="dateFormatter.formatValue(new Date(";
                    Double dou = (Double)dataFuel.get("ts");
                    
                    Long lon = dou.longValue()*1000;
                    
                    ss+= lon;
                    ss+="))";
                    ss+=",\n";   ////////////////////////////
                    ss+=dataFuel.get("v");
                    ss+="]\n";
                    if (i<rdataFuel.size()-1) ss+=",";
                }
                ss+="]";
                
                
 
//                    ss = rdataFuel.toString();
//                    ss=ss.replace("{", "[");
//                    ss=ss.replace("}", "]");
//                    ss=ss.replace("ts=", "");
//                    ss=ss.replace("v=", "");
//                    ss=ss.replace("null", "0");
                    
                    System.out.println("SSS " + ss);
                    return ss ;
                
        
    }
    
    public ArrayList<NavixyTrackerState> getState() {
    //public NavixyTrackerState getState() {

        System.out.println("################################################################");
        System.out.println("###########################   STATE   ##########################");
        System.out.println("################################################################");
        
        NavixyUser navixyUser;

                ArrayList<NavixyTrackerState> arrayList = new ArrayList<>();
        try {
            
            
            Map<String, Object> user = NavixyAPICalls.navixyAPILogin("test@DGA.com", "1234567");
            
            navixyUser = new Gson().fromJson(user.toString(), new TypeToken<NavixyUser>() {
            }.getType());        
            
            System.out.println("LOGIN USER HASH " + navixyUser.toString());
            
            ArrayList<NavixyTracker> navixyTrackers = null;
            
            navixyTrackers = listNavixyTrackers( navixyGetTrackers(navixyUser.getHash()));
        NavixyTracker navixyTracker = navixyTrackers.get(0);
        String trackerID = navixyTracker.getId();
            
        NavixyTrackerState trackerState = null;
        
                    trackerState = (NavixyTrackerState)NavixyAPICalls.navixyTrackerGetState(navixyUser.getHash(), trackerID);
        arrayList.add(trackerState);

            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WelcomeBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    




        return arrayList;
        //return trackerState;
    }   
    
}
