/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.dga.web;

import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Luis Caamaño
 */
@ManagedBean(name = "navigationController", eager = true)
@ViewScoped
@Dependent


public class NavigationController implements Serializable {
   @ManagedProperty(value = "#{param.passcode}")
   private String passcode;
    /**
     * Creates a new instance of NavigationController
     */
    
        public String pageChanger(){
            System.out.println("EEE##################\n"
                    + "passcode " + passcode);
            if(passcode.equals("aurora")) {
                return "pagina2";
            }
        return "error";
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }
}
    

