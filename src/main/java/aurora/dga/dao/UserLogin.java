/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.dga.dao;

/**
 *
 * @author Luis Caamaño
 */
public class UserLogin {

    public UserLogin() {
    }

    public UserLogin(String login, String firstName, String lastName, String clientType, String address) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.clientType = clientType;
        this.address = address;
    }
    
    
    
    private String login;
    private String firstName;
    private String lastName;
    private String clientType;
    private String address;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
            
}
