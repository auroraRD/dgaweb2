/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.dga.dao;

/**
 *
 * @author Luis Caamaño
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataConnect {

	public static Connection getConnection() {
            
         String usuario = "recomtac";
         String baseDatos = "recomtacdb";
         String password = "auroradga";
         String servidorBD = "apinav.rastreo.gs";
            
	        String url = "jdbc:postgresql://"+servidorBD+":5432/"+baseDatos;	
            try {
                
                
                    
			//Class.forName("com.mysql.jdbc.Driver");
			Class.forName("org.postgresql.Driver");
			Connection con = DriverManager.getConnection(
                                
					url, usuario, password);
			return con;
		} catch (Exception ex) {
			System.out.println("Database.getConnection() Error -->"
					+ ex.getMessage());
			return null;
		}
	}

	public static void close(Connection con) {
		try {
			con.close();
		} catch (Exception ex) {
		}
	}
}