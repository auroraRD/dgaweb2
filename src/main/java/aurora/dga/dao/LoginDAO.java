/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aurora.dga.dao;

/**
 *
 * @author Luis Caamaño
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import aurora.dga.dao.DataConnect;

public class LoginDAO {

	public static UserLogin validate(String user, String password) {
		Connection con = null;
		PreparedStatement ps = null;
                UserLogin userLogin = null;

		try {
			con = DataConnect.getConnection();
			ps = con.prepareStatement("Select login, password, firstname, lastname, address, client from public.\"User\"  where login=? and password=?");
			ps.setString(1, user);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				//result found, means valid inputs
                                String login = rs.getString("login");
                                String firstname = rs.getString("firstname");
                                String lastname = rs.getString("lastname");
                                String address = rs.getString("address");
                                String clienttype = rs.getString("client");
                                userLogin=new UserLogin(login, firstname, lastname, clienttype, address);
				return userLogin;
			}
		} catch (SQLException ex) {
			System.out.println("Login error -->" + ex.getMessage());
			return null;
		} finally {
			DataConnect.close(con);
		}
		return null;
	}
}